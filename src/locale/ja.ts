export default {
  Message: {
    Foo: 'foo',
    Bar: 'bar',
    Baz: 'baz',
    Goo: 'goo',
    Har: 'har',
    Haz: 'haz',
    Special: {
      Foo: 'foo',
      Bar: 'bar',
      Baz: 'baz',
      Goo: 'goo',
      Har: 'har',
      Haz: 'haz'
    }
  },
  Title: {
    Foo: 'foo',
    Bar: 'bar',
    Baz: 'baz',
    Test: 'Test Page',
    Goo: 'goo',
    Har: 'har',
    Haz: 'haz'
  },
  Placeholder: {
    Foo: 'foo',
    Bar: 'bar',
    Baz: 'baz',
    Goo: 'goo',
    Har: 'har',
    Haz: 'haz'
  },
  Global: {
    Foo: 'foo',
    Bar: 'bar',
    Baz: 'baz',
    Goo: 'goo',
    Har: 'har',
    Haz: 'haz',
    Settings: 'settings',
    File: 'file'
  },
  Testing: 'testing'
}
