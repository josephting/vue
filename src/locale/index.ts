import { createI18n } from 'vue-i18n'
import localeLangJa from './ja'

type FlattenObjectKeys<T extends Record<string, unknown>, Key = keyof T> = Key extends string
  ? T[Key] extends Record<string, unknown>
    ? `${Key}.${FlattenObjectKeys<T[Key]>}`
    : `${Key}`
  : never

const messages = {
  ja: localeLangJa
}

const i18n = createI18n<[typeof localeLangJa], 'ja'>({
  legacy: false,
  locale: 'ja',
  messages
})

export default i18n

export type LocaleKeys = FlattenObjectKeys<typeof localeLangJa>

export const t = (key: LocaleKeys) => i18n.global.t(key)
