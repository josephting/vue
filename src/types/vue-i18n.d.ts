// import { DefineLocaleMessage } from 'vue-i18n'
import localeLangJa from '../locale/ja'

type FlattenObjectKeys<T extends Record<string, unknown>, Key = keyof T> = Key extends string
  ? T[Key] extends Record<string, unknown>
    ? `${Key}.${FlattenObjectKeys<T[Key]>}`
    : `${Key}`
  : never

export type LocaleKeys = FlattenObjectKeys<typeof localeLangJa>

type LocaleLangJa = typeof localeLangJa

// declare module 'vue-i18n' {
//   export interface DefineLocaleMessage extends LocaleLangJa {}
// }
